<?php

include '../koneksi.php';

$resultArray = array();
$resultArray['data'] = [];
$query = "SELECT * FROM saldo WHERE 1=1 ";

if (isset($_GET['id_user'])) {
    $query = $query . "AND id_user = ". $_GET['id_user'];
}

$result = mysqli_query($conn, $query);
if ($result) {
    if (mysqli_num_rows($result) === 1) {
        $resultArray = array();
        while ($row = mysqli_fetch_array($result)) {
            $resultData = array();
            $resultData['total_harga'] = $row['total_bayar'];
            $resultData['total_sampah'] = $row['total_sampah'];
            $resultArray['data'][] = $resultData;
        }
    }
    
    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>