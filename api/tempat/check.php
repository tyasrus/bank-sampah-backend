<?php

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = [];

$query = "SELECT DATE(waktu) = DATE(NOW()) AS status_jemput FROM transaksi LEFT JOIN tempat ON transaksi.id_tempat = tempat.id WHERE tempat.id_user = " . $_GET['id'] . " ORDER BY waktu DESC LIMIT 1";
$result = mysqli_query($conn, $query);
if ($result) {
    $resultArray = array();
    $resultArray['data'] = [];
    while ($row = mysqli_fetch_array($result)) {
        $resultData = array();
        $resultData['status_jemput'] = $row['status_jemput'];
        $resultArray['data'][] = $resultData;
    }

    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);

?>