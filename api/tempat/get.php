<?php

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = "";
$query = "SELECT * FROM tempat WHERE 1=1 ";

if (isset($_GET['id'])) {
    $query = $query . "AND id = ". $_GET['id'];
}

if (isset($_GET['id_user'])) {
    $query = $query . "AND id_user = ". $_GET['id_user'];
}

$result = mysqli_query($conn, $query);
if ($result && mysqli_num_rows($result) === 1) {
    $resultArray = array();
    while ($row = mysqli_fetch_array($result)) {
        $resultData = array();
        $resultData['id'] = $row['id'];
        $resultData['name'] = $row['name'];
        $resultData['alamat'] = $row['alamat'];
        $resultData['latitude'] = $row['latitude'];
        $resultData['longitude'] = $row['longitude'];
        $resultArray['data'][] = $resultData;
    }

    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>
