<?php

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = "";

if (isset($_GET['id_user_level']) && $_GET['id_user_level'] == 1) {
    $query = "SELECT * FROM tempat";    
} else {
    $query = "SELECT DISTINCT te.* FROM tempat te LEFT JOIN (SELECT * FROM transaksi WHERE DATE(transaksi.waktu) = DATE(NOW())) tr ON te.id = tr.id_tempat WHERE tr.id_tempat IS NULL";
}

if (isset($_GET['id_user'])) {
    $query = $query . " AND id_user=" . $_GET['id_user'];
}

$result = mysqli_query($conn, $query);
if ($result) {
    $resultArray = array();
    if (mysqli_num_rows($result) >= 1) {
        $resultArray['rows'] = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
            $resultData = array();
            if (isset($_GET['latitude']) && isset($_GET['longitude'])) {
                if (distance($_GET['latitude'], $_GET['longitude'], $row['latitude'], $row['longitude']) <= 10) {
                    $resultData['id'] = $row['id'];
                    $resultData['name'] = $row['name'];
                    $resultData['alamat'] = $row['alamat'];
                    $resultData['latitude'] = $row['latitude'];
                    $resultData['longitude'] = $row['longitude'];
                    $resultArray['data'][] = $resultData;  
                }
            } else {
                    $resultData['id'] = $row['id'];
                    $resultData['name'] = $row['name'];
                    $resultData['alamat'] = $row['alamat'];
                    $resultData['latitude'] = $row['latitude'];
                    $resultData['longitude'] = $row['longitude'];
                    $resultArray['data'][] = $resultData;  
            }
        }
    } else {
        $resultArray['rows'] = 0;
        $resultArray['data'][] = "";
    }

    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

function distance($lat1, $lon1, $lat2, $lon2) {

  $theta = $lon1 - $lon2;   
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper("K");

    // if ($unit == "K") {
        return ($miles * 1.609344);
    // } else if ($unit == "N") {
    //     return ($miles * 0.8684);
    // } else {
    //     return $miles;
    // }
}

echo json_encode($resultArray);
?>
