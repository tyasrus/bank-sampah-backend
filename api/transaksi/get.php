<?php

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = '';
$query = "SELECT * FROM transaksi WHERE 1=1 ";
if ($_GET['id']) {
    $query = $query . "AND id=". $_GET['id'];
}

// if ($_GET['id_user']) {
//     $query = $query . "AND id_user = ". $_GET['id_user'];    
// }

// if ($_GET['id_user_pengguna']) {
//     $query = $query . "AND id_user = ". $_GET['id_user_pengguna'];    
// }


$result = mysqli_query($conn, $query);
if ($result) {
    $resultArray = array();
    while ($row = mysqli_fetch_array($result)) {
        $resultData = array();
        $resultData['id'] = $row['id'];
        $resultData['name'] = $row['name'];
        $resultData['alamat'] = $row['alamat'];
        $resultData['latitude'] = $row['latitude'];
        $resultData['longitude'] = $row['longitude'];
        $resultArray['data'][] = $resultData;
    }

    $resultArray['list_status'] = "success";
} else {
    $resultArray['list_status'] = "failed";
}

echo json_encode($resultArray);
?>
