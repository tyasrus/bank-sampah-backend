<?php

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = "";


$id = $_GET['id'];
$idUserLevel = $_GET['id_user_level'];

$query = "SELECT * FROM transaksi";


if ($idUserLevel === "2") {
    $query = "SELECT * FROM transaksi WHERE id_user=" . $_GET['id'];
}

if ($idUserLevel === "3") {
    $query = "SELECT * FROM transaksi INNER JOIN tempat ON transaksi.id_tempat = tempat.id WHERE tempat.id_user=" . $_GET['id'];
}


$result = mysqli_query($conn, $query);
if ($result) {
    $resultArray = array();
    if (mysqli_num_rows($result) >= 1) {
        $resultArray['rows'] = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
            $resultData = array();
            $resultData['id'] = $row['id'];
            $resultData['id_user'] = $row['id_user'];
            $resultData['id_tempat'] = $row['id_tempat'];
            $resultData['waktu'] = $row['waktu'];
            $resultData['total_harga'] = $row['total_harga'];
            $resultData['total_sampah'] = $row['total_sampah'];
            $resultData['status_bayar'] = $row['status_bayar'];
            $resultArray['data'][] = $resultData;
        }
    } else {
        $resultArray['rows'] = 0;
        $resultArray['data'][] = "";
    }

    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>
