<?php

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = "";

$id = $_POST['id'];
$idUser = $_POST['id_user'];
$idTempat = $_POST['id_tempat'];
$totalHarga = $_POST['total_harga'];
$totalSampah = $_POST['total_sampah'];

$query = "UPDATE tempat SET id_user = " . $idUser . ", id_tempat = " . $idTempat . ", total_harga = " . $totalHarga . ",
							total_sampah = " . $totalSampah . " WHERE id = " .  $id;

$result = mysqli_query($conn, $query);
if ($result) {
    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>