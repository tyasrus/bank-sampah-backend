<?php

/**
 * @Author: tyas
 * @Date:   2017-09-23 20:09:10
 * @Last Modified by:   tyas
 * @Last Modified time: 2017-09-30 15:55:01
 */

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = '';

$id = $_POST['id'];
$idLevel = $_POST['id_user_level'];
if ($idLevel == "3") {
    $query = "DELETE u, t, tr FROM user u JOIN tempat t ON u.id = t.id_user JOIN transaksi tr ON t.id = tr.id_tempat WHERE u.id = " .  $id;
} else {
    $query = "DELETE FROM user WHERE id = " . $id;
}


$result = mysqli_query($conn, $query);
if ($result) {
    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>