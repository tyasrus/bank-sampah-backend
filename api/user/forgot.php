<?php

/**
 * @Author: tyas
 * @Date:   2017-09-30 14:48:00
 * @Last Modified by:   tyas
 * @Last Modified time: 2017-09-30 16:23:10
 */

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = '';

if (isset($_POST['email'])) {
	$email = $_POST['email'];
	$query = "SELECT id FROM user WHERE email = '" . $email . "'";
	$result = mysqli_query($conn, $query);
	if ($result) {
		$resultArray = array();
	    while ($row = mysqli_fetch_array($result)) {
	        $resultData = array();
	        $resultData['id'] = $row['id'];
	        $resultArray['data'][] = $resultData;
	    }

	    $resultArray['status'] = "success";
	} else {
	    $resultArray['status'] = "failed";
	}
} else {
	$id = $_POST['id'];
	$password = $_POST['password'];

	$query = "UPDATE user SET password = '" . $password . "' WHERE id = " .  $id;

	$result = mysqli_query($conn, $query);
	if ($result) {
	    $resultArray['status'] = "success";
	} else {
	    $resultArray['status'] = "failed";
	}
}

echo json_encode($resultArray);
?>