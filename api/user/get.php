<?php

/**
 * @Author: tyas
 * @Date:   2017-09-30 15:23:06
 * @Last Modified by:   tyas
 * @Last Modified time: 2017-10-01 16:30:36
 */

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = '';
$list = "SELECT * FROM user WHERE id = ". $_GET['id'];

$result = mysqli_query($conn, $list);
if ($result) {
	$resultArray = array();
    while ($row = mysqli_fetch_array($result)) {
        $resultData = array();
        $resultData['id'] = $row['id'];
        $resultData['name'] = $row['name'];
        $resultData['email'] = $row['email'];
        $resultData['phone'] = $row['phone'];
        $resultData['password'] = $row['password'];
        $resultArray['data'][] = $resultData;
    }

    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>