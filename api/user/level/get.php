<?php

/**
 * @Author: tyas
 * @Date:   2017-10-01 09:21:49
 * @Last Modified by:   tyas
 * @Last Modified time: 2017-10-01 09:23:39
 */

include '../../koneksi.php';

$resultArray = array();
$resultArray['data'][] = '';
$query = "SELECT * FROM user_level WHERE id = ". $_GET['id'];

$result = mysqli_query($conn, $query);
if ($result) {
	$resultArray = array();
    while ($row = mysqli_fetch_array($result)) {
        $resultData = array();
        $resultData['id'] = $row['id'];
        $resultData['name'] = $row['name'];
        $resultArray['data'][] = $resultData;
    }

    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>