<?php

/**
 * @Author: tyas
 * @Date:   2017-10-01 09:22:11
 * @Last Modified by:   tyas
 * @Last Modified time: 2017-10-01 09:23:55
 */
include '../../koneksi.php';

$resultArray = array();
$resultArray['data'][] = '';
$query = "SELECT * FROM user_level WHERE id != 1";

$result = mysqli_query($conn, $query);
if ($result) {
	$resultArray = array();
    if (mysqli_num_rows($result) >= 1) {
        $resultArray['rows'] = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
            $resultData = array();
            $resultData['id'] = $row['id'];
            $resultData['name'] = $row['name'];
            $resultArray['data'][] = $resultData;
        }
    } else {
        $resultArray['rows'] = 0;
        $resultArray['data'][] = "";
    }

    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>