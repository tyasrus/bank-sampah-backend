<?php

/**
 * @Author: tyas
 * @Date:   2017-09-23 20:15:32
 * @Last Modified by:   tyas
 * @Last Modified time: 2017-10-01 09:30:36
 */

include '../koneksi.php';

$resultArray = array();
$resultArray['data'][] = "";

$query = "SELECT user.*, user_level.name AS name_level FROM user INNER JOIN user_level ON user.id_user_level = user_level.id WHERE user.id_user_level != 1";


$result = mysqli_query($conn, $query);
if ($result) {
    $resultArray = array();
    if (mysqli_num_rows($result) >= 1) {
        $resultArray['rows'] = mysqli_num_rows($result);
        while ($row = mysqli_fetch_array($result)) {
            $resultData = array();
            $resultData['id'] = $row['id'];
            $resultData['name'] = $row['name'];
            $resultData['email'] = $row['email'];
            $resultData['phone'] = $row['phone'];
            $resultData['id_user_level'] = $row['id_user_level'];
            $resultData['name_level'] = $row['name_level'];
            $resultArray['data'][] = $resultData;
        }
    } else {
        $resultArray['rows'] = 0;
        $resultArray['data'][] = "";
    }

    $resultArray['status'] = "success";
} else {
    $resultArray['status'] = "failed";
}

echo json_encode($resultArray);
?>
